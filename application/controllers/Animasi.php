<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Animasi extends MY_Controller {
  public $view    = 'view/animasi/';
  public $class   = 'animasi/';
	public function __construct(){
		parent::__construct();
    if ($this->session->userdata('status') != 'isLogin') {
      redirect('login');
    }
		$this->load->model('Animasi_model','model');
	}

	public function index(){
    $result['opt_biodata'] 	= options2('biodata', '', 'id', 'nama', '', '- Pilih -', '', array('id' => 'ASC'));
		$this->load_template('template', $this->view.'display', $result);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('biodata');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
	{
    $id = $this->input->post('id');
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('biodata');
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url($this->class.'save');
      $response['disable'] = 'disabled';
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['simpan'] = base_url($this->class.'save');
        $response['disable'] = '';
      }
    }else{
      $response['data'] = array();
      $response['simpan'] = base_url($this->class.'save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
	}

	public function save()
	{
		$where['id'] = $this->input->post('id');
    $data = array(
      'nama' => $this->input->post('nama'),
      'jabatan' => $this->input->post('jabatan'),
    );
    if ($where['id'] != '') {
      $update = $this->model->update_data('biodata', $data, $where);
      $response['pesan'] = 'Gagal Melakukan Update Biodata';
      $response['status'] = 404;
      if ($update) {
        $response['pesan'] = 'Berhasil Melakukan Update Biodata';
        $response['status'] = 200;
      }
    }else{
      $create = $this->model->create_data('biodata', $data);
      $response['pesan'] = 'Data Tidak Berhasil Ditambahkan';
      $response['status'] = 404;
  		if ($create) {
        $response['pesan'] = 'Data Berhasil Ditambahkan';
        $response['status'] = 200;
      }
    }
      echo json_encode($response);
	}

  public function update_list(){
    $posisi = $this->input->post('position');
    $i = 0;
    foreach ($posisi as $key => $value) {
      $i++;
      $this->master_model->update(['urutan' => $i], ['id_user' => $value], 'urutan_biodata');
    }
  }

	public function delete()
  {
    $where['id'] = $this->input->post('id');
    $status = $this->model->delete_data($where);
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($status) {
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

}
