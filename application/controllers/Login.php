<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {

    public $modul = 'login/';
    public $view = 'login/';
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function index(){
      if ($this->session->userdata('status') != 'isLogin') {
        $result['action'] = base_url($this->modul.'checkLogin');
        $this->load->view($this->view.'login', $result);
      }else{
        redirect('table');
      }
    }
    
    public function checkLogin()
    {
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $where = ['username' => $username, 'password' => $password];
      $sess = $this->master_model->data('*', 'user', $where)->get()->row();

      $response['pesan'] = 'Username atau password salah !';
      $response['status'] = 404;
      if (!empty($sess)) {
        $sess_data['id'] = $sess->id;
        $sess_data['status'] = 'isLogin';
        $this->session->set_userdata($sess_data);

        $response['pesan'] = 'Berhasil melakukan login';
        $response['status'] = 200;
        $response['url'] = site_url('table');
      }
      echo json_encode($response);
    }

    function logout(){
  		$this->session->sess_destroy();
  		redirect('login');
  	}

}
