<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model');
	}

	public function index(){
		$this->load_template('template', 'view/index');
	}

	public function get($condition = '')
	{
		$cond = $condition == '' ? $this->input->post('cond') : $condition;
		$this->model->id = $this->input->post('id');
		$get = $this->model->get('biodata');
		if ($get -> num_rows() > 0) {
	        $response['pesan'] = 'data ada';
	        $response['status'] = 200;
	        $response['data'] = $get->result_array();
	    } else {
	        $response['pesan'] = 'data tidak ada';
	        $response['status'] = 404;
	    }
			if ($cond == 'API') {
				echo json_encode($response);
			}else{
				$this->load->view('index', $response);
			}
	}

	public function create()
	{
		$data = array(
			'nama' => $this->input->post('nama'),
			'jabatan' => $this->input->post('jabatan'),
		);
		$create = $this->model->create('biodata', $data);
		if ($create) {
            $response['pesan'] = 'insert berhasil';
            $response['status'] = 200;
          } else {
            $response['pesan'] = 'insert error';
            $response['status'] = 404;
          }
		echo json_encode($response);
	}

	public function updatee()
	{
		$where['id'] = $this->input->post('id');
		$data = array(
			'nama' => $this->input->post('nama'),
			'jabatan' => $this->input->post('jabatan'),
		);
		$update = $this->model->update('biodata', $data, $where);
		if ($update) {
            $response['pesan'] = 'update berhasil';
            $response['status'] = 200;
          } else {
            $response['pesan'] = 'update error';
            $response['status'] = 404;
          }
		echo json_encode($response);
	}

	public function delete()
    {
          $where['id'] = $this->input->post('id');
          $status = $this->model->delete($where);
          if ($status == true) {
            $response['pesan'] = 'hapus berhasil';
            $response['status'] = 200;
          } else {
            $response['pesan'] = 'hapus error';
            $response['status'] = 404;
          }
          echo json_encode($response);
    }

}
