<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	public function get($table)
	{
		$this->db->select('a.*, b.urutan');
		$this->db->from($table.' a');
		$this->db->join('urutan_biodata b', 'a.id = b.id_user', 'LEFT');
		if ($this->id != '') $this->db->where('id', $this->id);
		$this->db->order_by('b.urutan', 'ASC');
		return $this->db->get();
	}

	public function create($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function update($table, $data, $where)
	{
		$this->db->from($table);
		$this->db->set($data);
		$this->db->where($where);
		return $this->db->update();
	}

	public function delete($where){
		$this->db->where($where);
        return $this->db->delete('biodata');
	}

}
